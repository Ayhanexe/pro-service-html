$(document).ready(() => {
  const sidebarMenu = $(".sidebar-menu");
  const filter = $(".filter");
  $(".menu-icon").click(() => {
    $(sidebarMenu).hasClass("active")
      ? $(sidebarMenu).removeClass("active")
      : $(sidebarMenu).addClass("active");
    if ($(filter).hasClass("disabled")) {
        $(filter).removeClass("disabled");
      $(filter).animate(
        {
          opacity: 1,
        },
        {
          duration: 300
        }
      );
    } else {
      $(filter).animate(
        {
          opacity: 0,
        },
        {
          duration: 300,
          complete: () => {
            $(filter).addClass("disabled");
          },
        }
      );
    }
  });
  $(".filter").click(() => {
      $(filter).animate(
        {
          opacity: 0,
        },
        {
          duration: 300,
          complete: () => {
            $(filter).addClass("disabled");
          },
        }
      );
    $(sidebarMenu).removeClass("active");
  });
});
