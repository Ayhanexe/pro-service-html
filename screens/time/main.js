$(document).ready(() => {
    const calendar_items = $(".calendar-table td");
    const time_items = $(".time-item")
    
    $(calendar_items).click(function() {
        $(calendar_items).removeClass("active");
        $(this).addClass("active");
    });
    
    $(time_items).click(function() {
        $(time_items).removeClass("active");
        $(this).addClass("active");
    });
    
});